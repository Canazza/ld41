﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioClipEvents : MonoBehaviour {
    public AudioSource source;
    public UnityEvent OnStop;
    private bool wasPlayingLastFrame = false;
	// Use this for initialization
	void Start () {
        if (source == null) source = this.GetComponent<AudioSource>();
        wasPlayingLastFrame = source.playOnAwake;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(source.isPlaying != wasPlayingLastFrame)
        {
            Debug.Log("Is playing" + source.isPlaying);
            Debug.Log("was playing" + wasPlayingLastFrame);
        }
        if (!source.isPlaying && wasPlayingLastFrame)
        {
            OnStop.Invoke();
        }
        wasPlayingLastFrame = source.isPlaying;
	}
}
