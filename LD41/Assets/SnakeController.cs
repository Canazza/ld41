﻿using com.clydebuiltgames.Pooling;
using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SnakeController : MonoBehaviour {
    public float WaitBetweenUpdates { get { return SpeedPerLevel[Level]; } }
    public int TypesToUse { get { return ItemsToUsePerLevel[Level]; } }
    public float GridSize = 1;
    public int GridWidth = 15;
    public int GridHeight = 10;
    public int Score = 0;
    public float[] SpeedPerLevel;
    public int[] ItemsToUsePerLevel;
    public int[] LevelThresholds;
    public int Level = 0;
    public int ScorePerMatch;
    public int X, Y;
    public Vector2Int LastDirection;
    public Vector2Int Direction;
    public Transform Graphics;
    public List<Vector2> TrailPositions;
    public List<TrailType> TrailTypes;
    public List<Trail> Trails;
    public List<Pickup> Pickups;
    public Spawner PickupSpawner, TrailSpawner, ScorePopupSpawner;
    public UnityEvent OnDeath, OnPickup, OnTailDestroy;
    public IntEvent OnScore, OnCombo,OnLevel,OnNextLevel;
    [System.Serializable]
    public class Trail
    {
        public TrailType Type;
        public GameObject GameObject;
    }
    [System.Serializable]
    public class TrailType
    {
        public Color Color;

    }
    [System.Serializable]
    public class Pickup
    {
        public int X, Y;
        public GameObject GameObject;
        public TrailType Type;
    }
    public void SetX(int x)
    {
        if ((x != 0 && (LastDirection.y != 0)) || (Direction.x == 0 && Direction.y == 0))
        {
            Direction.y = 0;
            Direction.x = Mathf.Clamp(x, -1, 1);
        }
    }
    public void SetY(int y)
    {
        if ( (y != 0 && (LastDirection.x != 0)) || (Direction.x == 0 && Direction.y == 0))
        {
            Direction.x = 0;
            Direction.y = Mathf.Clamp(y, -1, 1);
        }
    }
    private void OnDrawGizmos()
    {
        for(int x = -GridWidth; x < GridWidth; x += 1)
        {
            for (int y = -GridHeight; y < GridHeight; y += 1)
            {
                Gizmos.DrawWireCube(new Vector3(x + 0.5f, y, 0) * GridSize, Vector3.one * GridSize);
            }
        }
    }
    // Use this for initialization
    void Start ()
    {
        OnLevel.Invoke(Level + 1);
        OnNextLevel.Invoke(LevelThresholds[Level]);
        OnScore.Invoke(0);
        OnCombo.Invoke(1);
        LastDirection = Direction;
        StartCoroutine(Movement());
        StartCoroutine(SpawnPickup());
        //StartCoroutine(SpawnPickup());
        //StartCoroutine(SpawnPickup());
        TrailPositions = new List<Vector2>(100);
	}
    public Vector2 WorldFromGridPosition(int x, int y)
    {
        return new Vector2(x * GridSize - 1f, y * GridSize + 0.5f);
    }
    [ContextMenu("Create Pickup")]
	public void CreatePickup(int x = 0, int y = 0)
    {
        var pickup = new Pickup()
        {
            X = x,
            Y = y,
            GameObject = PickupSpawner.Spawn(),
            Type = TrailTypes.Take(TypesToUse).OrderBy(tx => Random.Range(0,100)).First()
        };
        pickup.GameObject.GetComponent<SpriteRenderer>().color = pickup.Type.Color;
        pickup.GameObject.transform.localPosition = WorldFromGridPosition(pickup.X, pickup.Y);
        Pickups.Add(pickup);
    }
    private bool CanSpawnPickup(int x, int y)
    {
        if (x <= -GridWidth || x > GridWidth || y < -GridHeight || y >= GridHeight)
        {
            return false;
        }
        if (Mathf.Abs(X - x) < 3 && Mathf.Abs(Y - y) < 3) return false;
        foreach(var pickup in Pickups)
        {
            if (pickup.X == x && pickup.Y == y) return false;
        }
        return true;
    }
    int combo = 0;
    IEnumerator CheckMatch3()
    { 
        var leftToCheck = Trails.ToList();
        var toRemove = new List<Trail>(); 
        foreach(var trail in Trails)
        {
            if (!leftToCheck.Contains(trail)) continue; 
            leftToCheck.Remove(trail);
            var similar = Trails.Where(x => x.Type == trail.Type && x.GameObject != trail.GameObject).ToList();
            var found = FindAdjacent(trail, similar);
            found.Insert(0,trail);
            if (found.Count > 2)
            {
                combo += Mathf.Clamp(found.Count - 2,1,255);
                toRemove.AddRange(found);
                Debug.Log("Score: " + toRemove.Count+" * "+ScorePerMatch+" * "+ combo + " = "+ (toRemove.Count *ScorePerMatch* combo));
                Score += toRemove.Count * ScorePerMatch * combo;
                var popup = ScorePopupSpawner.Spawn();
                popup.GetComponentInChildren<Text>().text = string.Format("{0}", toRemove.Count * ScorePerMatch * combo);
                OnScore.Invoke(Score);
                OnCombo.Invoke(combo);
                foreach(var foundItem in found)
                {
                    StartCoroutine(DestroyTrailItem(foundItem.GameObject, 0.5f));
                }
                OnTailDestroy.Invoke();
                yield return new WaitForSeconds(0.5f); 
                leftToCheck.RemoveAll(x => found.Contains(x));

            }
        }
        Trails.RemoveAll(x=> toRemove.Contains(x));
         
        yield break;
    }
    private List<Trail> FindAdjacent(Trail item, List<Trail> Matching )
    { 
        var found = new List<Trail>();
        var remaining = Matching.ToList();
        for(int i = 0; i < Matching.Count; i ++)
        {
            var match = Matching[i];
            remaining.Remove(match);
            var distance = Vector2.Distance(match.GameObject.transform.position, item.GameObject.transform.position); 
            if(distance <= GridSize)
            { 
                found.Add(match);
                var adjacent = FindAdjacent(match, remaining);
                remaining.RemoveAll(x => adjacent.Contains(x));
                found.AddRange(adjacent);
            }
        }
        return found;
    }
    
    IEnumerator SpawnPickup()
    {
        yield return new WaitForSeconds(1);
        int x, y;
        do
        {
            x = Random.Range(-GridWidth, GridWidth);
            y = Random.Range(-GridHeight, GridHeight);
        }
        while (!CanSpawnPickup(x, y));
        CreatePickup(x,y);
    }
    IEnumerator CheckCollisions()
    {
        if(X <= -GridWidth || X > GridWidth || Y < -GridHeight || Y >= GridHeight)
        {
            Debug.Log("OUT OF BOUNDS");
            OnDeath.Invoke();
            yield break;
        }
        for(int i = 0; i < Trails.Count; i++)
        {
            var dist = Vector2.Distance(Trails[i].GameObject.transform.position, this.transform.position);
            if (dist <= 0.1f)
            {
                Debug.Log("COLLIDE WITH TRAIL:" + dist + ", " + Trails[i].GameObject.transform.position);
                OnDeath.Invoke();
                yield break;
            }
        }
        foreach (var pickup in Pickups)
        {
            if(pickup.X == X && pickup.Y == Y)
            {
                Pickups.Remove(pickup);
                pickup.GameObject.SetActive(false);
                var trail = new Trail()
                {
                    GameObject = TrailSpawner.Spawn(),
                    Type = pickup.Type
                };
                trail.GameObject.GetComponent<SpriteRenderer>().color = pickup.Type.Color;
                trail.GameObject.transform.position = this.transform.position;
                trail.GameObject.transform.localScale = Vector3.one;
                trail.GameObject.SetActive(true);
                Trails.Add(trail);

                StartCoroutine(SpawnPickup());
                combo = 0;
                OnCombo.Invoke(1);
                OnPickup.Invoke();
                yield break;
            }
        }
    }
    IEnumerator Movement()
    {
        TrailPositions = new List<Vector2>();
        TrailPositions.Add(this.transform.position);
        while (true)
        {
            yield return new WaitForSeconds(WaitBetweenUpdates);

            TrailPositions.Add(this.transform.position);
            X += Direction.x;
            Y += Direction.y;
            LastDirection = Direction;
            this.transform.localPosition = WorldFromGridPosition(X, Y);
            Graphics.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(Direction.y, Direction.x) * Mathf.Rad2Deg);

            UpdateTrails();
            yield return CheckCollisions();

            while (TrailPositions.Count > Trails.Count)
            {
                TrailPositions.RemoveAt(0);
            }

            UpdateTrails();
            yield return CheckMatch3();
            UpdateTrails();
            yield return CheckMatch3();
            UpdateTrails();
            yield return CheckMatch3();
            UpdateTrails();
            yield return CheckMatch3();
            UpdateTrails();
            if (Level < LevelThresholds.Count())
            {
                OnNextLevel.Invoke(LevelThresholds[Level] - Score);
            }
            while (Level < LevelThresholds.Count() && Score >= LevelThresholds[Level])
            {
                Level++;
                var levelUp = ScorePopupSpawner.Spawn();
                levelUp.GetComponentInChildren<Text>().text = "Level Up!";
                yield return new WaitForSeconds(0.5f);
                StartCoroutine(SpawnPickup());
                StartCoroutine(SpawnPickup());
                OnLevel.Invoke(Level + 1);
            }
        }
    }
    public AnimationCurve DestroyCurve;
    IEnumerator DestroyTrailItem(GameObject g, float time)
    {
        for(float t = 0; t < 1; t += Time.deltaTime / time)
        {
            g.transform.localScale = Vector3.LerpUnclamped(Vector3.zero, Vector3.one, DestroyCurve.Evaluate(t));
            yield return null;
        }
        g.SetActive(false);
    }
    void UpdateTrails()
    {

        for (int i = 0; i < Trails.Count; i++)
        {
            var position = TrailPositions[TrailPositions.Count - i - 1];
            Trails[i].GameObject.transform.position = position;
        }
    }
}
