﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour {
    public float Countdown;
    public UnityEvent OnTimer, OnTimerStart;
    public bool StartOnEnabled;
    private void OnEnable()
    {
        if(StartOnEnabled)
        {
            StartTimer();
        }
    }

    public void StartTimer()
    {
        OnTimerStart.Invoke();
        Invoke("InvokeTimer", Countdown);
    }
    
    private void InvokeTimer()
    {
        OnTimer.Invoke();
    }
     
}
