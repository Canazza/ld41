﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextFormatter : MonoBehaviour {
    public Text Text;
    public string Format;
    public string InitialText;
    private string text;
    public void SetText(int number)
    {
        text = string.Format(Format, number);
        if (Text != null)
        {
            Text.text = string.Format(Format, number);
        }  
    }
    public void SetText(float number)
    {
        text = string.Format(Format, number);
        if (Text != null)
        {
            Text.text = string.Format(Format, number);
        }
    }
	// Use this for initialization
	void Awake () {
        Text = Text ?? this.GetComponent<Text>();
        Format = string.IsNullOrEmpty(Format) ? Text.text : Format;
        Text.text = InitialText;
	}
    private void OnEnable()
    {
        Text.text = text;

    }
}
